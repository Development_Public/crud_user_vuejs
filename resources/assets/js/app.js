require('./bootstrap');

window.Vue = require('vue');

Vue.component('my-thoughts-component', require('./components/MyThoughtsComponent.vue'));
Vue.component('form-component', require('./components/FormComponent.vue'));
Vue.component('thought-component', require('./components/ThoughtComponent.vue'));

Vue.component('my-user-component', require('./components/MyUserComponent.vue'));
Vue.component('user-form-component', require('./components/UserFormComponent.vue'));
Vue.component('user-component', require('./components/UserComponent.vue'));

const app = new Vue({
    el: '#app'
});
