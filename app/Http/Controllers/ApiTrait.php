<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

trait ApiTrait
{
    public function index()
    {
        $query = $this->model->all();
        return response()->json($query);
    }

    public function store(Request $request)
    {
        $query = $this->model->create($request->all());
        $query->save();

        return response()->json($query);
    }

    public function show($id)
    {
        $query = $this->model->find($id);
        return response()->json($query);
    }

    public function update(Request $request, $id)
    {
        $query = $this->model->find($id);
        $query->update($request->all());
        return response()->json($query);
    }

    public function destroy($id)
    {
        $query =$this->model->find($id);
        $query->delete();
        return response()->json($query);
    }

}
