<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function __construct(User $user)
    {
       $this->model = $user;
    }

    use \App\Http\Controllers\ApiTrait;

    private $model;

}
